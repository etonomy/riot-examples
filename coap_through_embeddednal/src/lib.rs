#![no_std]
//! This example creates a CoAP server (both on UDP and TCP), and provides some command line
//! interaction.
//!
//! It resembles the gcoap RIOT example, but does not contain the shell (and thus CoAP client)
//! part.

use riot_wrappers::{
    riot_main_with_tokens,
    thread,
    stdio::println,
    msg,
    gnrc::{self, netreg},
};
use riot_wrappers::msg::v2::MessageSemantics;

use embedded_nal::{UdpClientStack, UdpFullStack, nb, TcpFullStack, TcpClientStack};
use embedded_hal::blocking::delay::DelayMs;
use coap_handler::Attribute;
use coap_handler_implementations::{HandlerBuilder, ReportingHandlerBuilder};

// Wrap main into a C function named `int main(void)` function that is invoked as the main thread
// function by RIOT.
riot_main_with_tokens!(outer_main);

// Using an "outer" to not re-indent the whole main function
fn outer_main(initial: thread::StartToken) -> ((), thread::TerminationToken) {
    initial.with_message_queue::<4, _>(|initial| {
        // Doing this in the closure because the current type if `initial` can't be named, and that
        // would be required when using it with a function
        let (_, semantics) = initial.take_msg_semantics();
        main(semantics)
    })
}

fn main(msg_semantics: msg::v2::NoConfiguredMessages) -> ! {
    println!("Preparing CoAP server");

    #[cfg(feature = "microbit")]
    let mut display = riot_wrappers::microbit::LEDs::new();
    #[cfg(feature = "microbit")]
    {
        use embedded_graphics::{prelude::*, primitives::*, pixelcolor::BinaryColor, style::*};
        let circle = Circle::new(Point::new(2, 2), 2)
            .into_styled(PrimitiveStyle::with_fill(BinaryColor::On));
        circle.draw(&mut display).unwrap();
    }

    let mut stack: riot_wrappers::socket_embedded_nal::Stack<1> = riot_wrappers::socket_embedded_nal::Stack::new();

    let pings: riot_wrappers::mutex::Mutex<riot_coap_handler_demos::ping::PingPool> = Default::default();

    stack.run(|mut stack| {
        let handler = coap_message_demos::full_application_tree(None)
            .below(&["ps"], riot_coap_handler_demos::ps::ps_tree())
            .below(&["ping"], riot_coap_handler_demos::ping::ping_tree(&pings))
            .at_with_attributes(&["stdio", "write"], &[Attribute::Title("POST text for stdout")], riot_coap_handler_demos::stdio::write())
            .at_with_attributes(&["stdio", "read"], &[], riot_coap_handler_demos::stdio::read())
            ;

        #[cfg(feature = "i2c")] // ... but with which? Could be configured numerically
        let handler = handler
            .at(&["i2c", "0"], riot_coap_handler_demos::i2c::handler_for(riot_wrappers::i2c::I2CDevice::new(0)))
            ;
        #[cfg(feature = "gpio")]
        assert!(riot_wrappers::board() == "microbit-v2", "GPIO configuration is hard-wired to microbit-v2");
        #[cfg(feature = "gpio")] // ... but with which? Really weird to configure as we can't evaluate BTN0_PIN
        let handler = handler
            .at_with_attributes(
                    &["gpio", "0"],
                    &[Attribute::Title("Button A")],
                    riot_coap_handler_demos::gpio::handler_for_input(
                        riot_wrappers::gpio::GPIO::from_c(14 /* pin 0.14: microbit button a */)
                            .unwrap()
                            .configure_as_input(riot_wrappers::gpio::InputMode::In)
                            .unwrap()
                            ))
            .at_with_attributes(
                    &["gpio", "1"],
                    &[Attribute::Title("Button B")],
                    riot_coap_handler_demos::gpio::handler_for_input(
                        riot_wrappers::gpio::GPIO::from_c(23 /* pin 0.23: microbit button b */)
                            .unwrap()
                            .configure_as_input(riot_wrappers::gpio::InputMode::In)
                            .unwrap()
                            ))
            ;

        #[cfg(feature = "microbit")]
        let handler = handler
            .at_with_attributes(
                &["canvas", ""],
                &[Attribute::Title("Microbit LED matrix"), Attribute::Ct(0), Attribute::Sz(25)],
                riot_coap_handler_demos::graphics::handler_for(
                    display
                    ));

        let handler = handler
            .with_wkc()
            ;

        let mut handler = handler;

        let stack_tcp: riot_wrappers::socket_embedded_nal_tcp::ListenStack<4> = Default::default();
        pin_utils::pin_mut!(stack_tcp);
        let stack_tcp = &mut stack_tcp;

        // Currently dysfunctional -- we're only waiting for the one UDP socket (but while we ping
        // things should work at least with the delays)
        let mut tcpsock = stack_tcp.socket().unwrap();
        stack_tcp.bind(&mut tcpsock, 5683).unwrap();
        let mut tcppool = embedded_nal_minimal_coaptcpserver::ServerPool::<_, 4, 1152>::new(tcpsock);

        let mut sock = stack.socket().unwrap();
        stack.bind(&mut sock, 5683).unwrap();

        const FLAG0: u16 = (1 << 0);
        // Ensure that any activity on that will wake up our thread
        //
        // The socket is only available after having been bound (because of concept mismatches
        // between embedded-nal and RIOT concepts)
        extern "C" fn set_flag0(sock: *mut riot_sys::sock_udp, flags: riot_sys::sock_async_flags_t, arg: *mut riot_sys::libc::c_void) {
            let mut target_pid: i16 = arg as _;
            let thread = unsafe  { riot_sys::thread_get(target_pid) };
            // Would be the safer conversion, but it's not public in riot-wrappers.
            // let thread = riot_wrappers::inline_cast_mut(thread);
            let thread = thread as _;
            unsafe { riot_sys::thread_flags_set(thread, FLAG0) };
        }
        let my_pid: i16 = riot_wrappers::thread::get_pid().into();
        unsafe { riot_sys::sock_udp_set_cb(
                sock.socket().unwrap(),
                Some(set_flag0),
                my_pid as _,
                ) };

        let (msg_semantics, recv_rx, recv_tx) = msg_semantics.split_off();

        netreg::register_for_messages(
            recv_tx,
            riot_sys::gnrc_nettype_t_GNRC_NETTYPE_ICMPV6,
            gnrc::icmpv6::EchoType::Reply.into(),
            || {
                let mut ping_timer = Default::default();

                loop {
                    let flags_seen = unsafe { riot_sys::thread_flags_wait_any(
                            FLAG0
                            | riot_sys::THREAD_FLAG_TIMEOUT as u16
                            | riot_sys::THREAD_FLAG_MSG_WAITING as u16
                            ) };

                    if flags_seen & riot_sys::THREAD_FLAG_TIMEOUT as u16 != 0 {
                        // Ping intervals currently work oddly: They're all in lockstep, on a single
                        // timeout, and happen here whenever the flag is set. We're hoping to send the
                        // requests soon after the interval fired (we don't want to afford keeping a
                        // full timestamp for each entry in the window, so we'll just assume it's all sent
                        // at the tick start)
                        //
                        // As this is used right now, this can not block (as all mutex references
                        // are in this thread). When gcoap is used, this may block for a moment
                        // until the Gcoap handler is done with whatever it is just doing wcjith
                        // the pings.
                        pings.lock().tick();
                    }

                    // Not looking at the message flag here: We woke up, so we exhaust the list;
                    // same goes for the socket stuff.

                    while let Some(msg) = msg_semantics.try_receive() {
                        msg.decode(&recv_rx, |_sender, packet| {
                            // blocking: See above
                            pings.lock().received(packet);
                        });
                    }

                    loop {
                        match embedded_nal_minimal_coapserver::poll(&mut stack, &mut sock, &mut handler) {
                            // After we've cleared the flag, we have to exhaust anything that's
                            // pending
                            Ok(_) => continue,
                            Err(nb::Error::WouldBlock) => break,
                            e => e.unwrap(),
                        }
                    }

                    tcppool.poll(stack_tcp, &mut handler)
                        .expect("Actual error in polling (accepting?)");

                    // This is a crude approximation of a periodic timer; in a later stage
                    // we'll want this to use ztimer_periodic for accuracy.
                    //
                    // Blocking: see above
                    if pings.lock().any_active_now() {
                        unsafe { riot_sys::ztimer_set_timeout_flag(riot_sys::ZTIMER_MSEC, &mut ping_timer, 1000) };
                    }
                }
        });
    });

    unreachable!()
}
