#![no_std]

use riot_wrappers::riot_main;
use riot_wrappers::stdio::println;

use embedded_hal::blocking::delay::DelayMs;

use heapless::{consts::U8, Vec};

use riot_wrappers::{saul, ztimer};

use riot_wrappers::saul::registration::{register_and_then, Drivable, Driver, Error};

// Wrap main into a C function named `int main(void)` function that is invoked as the main thread
// function by RIOT.
riot_main!(main);

static DRIVER: Driver<SoftwareOnlyLED> = Driver::new();

fn main() -> Result<(), core::fmt::Error> {
    let mut delay = ztimer::Clock::msec();

    // Part one: Provide a LED via SAUL

    let softled = SoftwareOnlyLED("The Software LED");
    register_and_then(&DRIVER, &softled, None, || {
        // Part two: Use this LED -- and any others -- to toggle something around

        let mut leds = find_leds();

        assert!(leds.len() != 0, "There are no Lights.");

        loop {
            for l in leds.iter_mut() {
                l.blink();
                delay.delay_ms(500);
            }
        }
    });
}

enum Actionable {
    Switch(saul::RegistryEntry),
    Dimmer(saul::RegistryEntry),
    Rgb(saul::RegistryEntry),
}

impl Actionable {
    /// Do something with the SAUL actuator inside self, typically blink, for around 0.5s
    fn blink(&self) {
        let mut delay = ztimer::Clock::msec();
        match self {
            Actionable::Switch(l) => {
                let on = saul::Phydat::new(&[1], Some(saul::Unit::Bool), 0);
                let off = saul::Phydat::new(&[0], Some(saul::Unit::Bool), 0);

                l.write(on).unwrap();
                delay.delay_ms(500);
                l.write(off).unwrap();
            }
            Actionable::Dimmer(l) => {
                // Not unwrapping: For some reason this returns 3
                let set = |v| l.write(saul::Phydat::new(&[v], Some(saul::Unit::Percent), 0));
                for i in (0..=100).chain((0..=100).rev()) {
                    set(i);
                    delay.delay_ms(3);
                }
            }
            Actionable::Rgb(l) => {
                let set = |r, g, b| {
                    l.write(saul::Phydat::new(&[r, g, b], Some(saul::Unit::Percent), 0))
                        .unwrap()
                };
                for i in 0..=100 {
                    set(i, 0, 0);
                    delay.delay_ms(1);
                }
                // Spending a bit more time here to compensate for the outer colors being more
                // noticable
                for i in 0..=100 {
                    set(100 - i, i, 0);
                    delay.delay_ms(2);
                }
                for i in 0..=100 {
                    set(0, 100 - i, i);
                    delay.delay_ms(2);
                }
                for i in 0..=100 {
                    set(0, 0, 100 - i);
                    delay.delay_ms(1);
                }
            }
        }
    }
}

/// Look though all currently registered SAUL items, and return the ones that are LEDs as Vec of up
/// to 8 items. (Actually it's going through all switches, hoping they are LEDs).
///
/// The working assumption behind the &'static is that LEDs never go away.
fn find_leds() -> Vec<Actionable, U8> {
    let mut result = Vec::new();

    for current in saul::RegistryEntry::all() {
        let item = match current.type_() {
            Some(saul::Class::Actuator(Some(saul::ActuatorClass::Switch))) => {
                Actionable::Switch(current)
            }
            Some(saul::Class::Actuator(Some(saul::ActuatorClass::Dimmer))) => {
                Actionable::Dimmer(current)
            }
            Some(saul::Class::Actuator(Some(saul::ActuatorClass::LedRgb))) => {
                Actionable::Rgb(current)
            }
            _ => continue,
        };

        // Would be nice to say what it is, but not worth implementing Debug manualy
        // ... or to say anything at all, why is RegistryEntry not Clone?
        // println!("Found suitable thing {}, currently {:?}", current.name().unwrap_or("(unnamed"), current.read());
        result
            .push(item)
            // Silently ignore overflow, blink only the first 8 LEDs
            .ok();
    }

    result
}

struct SoftwareOnlyLED(&'static str);

impl Drivable for &SoftwareOnlyLED {
    const CLASS: saul::Class = saul::Class::Actuator(Some(saul::ActuatorClass::Switch));

    const HAS_WRITE: bool = true;

    fn write(self, value: &saul::Phydat) -> Result<u8, Error> {
        println!("Setting {} to {:?}", self.0, value);
        Ok(1)
    }
}
