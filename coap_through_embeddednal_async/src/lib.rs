#![no_std]
//! This example creates a CoAP server (both on UDP and TCP), and provides some command line
//! interaction.
//!
//! It resembles the gcoap RIOT example, but does not contain the shell (and thus CoAP client)
//! part.

use riot_wrappers::{riot_main, stdio::println};

use coap_handler_implementations::ReportingHandlerBuilder as _;

use embedded_nal_async::{SocketAddr, UdpStack as _};
use static_cell::StaticCell;

riot_main!(main);

fn main() -> ! {
    // Could also use static-on-stack crate instead
    static EXECUTOR: StaticCell<embassy_executor_riot::Executor> = StaticCell::new();
    let executor: &'static mut _ = EXECUTOR.init(embassy_executor_riot::Executor::new());
    executor.run(|spawner| {
        spawner.spawn(amain(spawner)).unwrap();
    })
}

#[embassy_executor::task]
async fn amain(spawner: embassy_executor::Spawner) {
    // The simplest we can do right now; adding application doesn't get us any new fanciness,
    // because coap-handler is blocking.
    let handler = coap_message_demos::full_application_tree(None);
    let mut handler = handler.with_wkc();

    static UDP_SOCKET: StaticCell<riot_sys::sock_udp_t> = StaticCell::new();
    let stack =
        riot_wrappers::socket_embedded_nal_async_udp::UdpStack::new(|| UDP_SOCKET.try_uninit());

    let mut sock = stack
        .bind_multiple(SocketAddr::new("::".parse().unwrap(), 5683))
        .await
        .expect("Can't create a socket");

    let coap = embedded_nal_coap::CoAPShared::<3>::new();
    // FIXME: If we really don't use client, we could use a simpler CoaPShared
    let (client, server) = coap.split();

    embassy_futures::join::join(
        async {
            server
                .run(
                    &mut sock,
                    &mut handler,
                    &mut riot_wrappers::random::Random::new(),
                )
                .await
                .expect("UDP error")
        },
        // It'd be tempting to spawn that as a task, but there we'd run into lifetime trouble.
        // Nothing insurmountable, but running in one task is just as easy right now.
        client_activities(client)
    ).await;
}

async fn client_activities(client: embedded_nal_coap::CoAPRuntimeClient<'_, 3>) {
    println!("Waiting a moment before sending requests");

    riot_wrappers::ztimer::Clock::msec().sleep_async(riot_wrappers::ztimer::Ticks(4000)).await;

    let demoserver = "[2a01:4f8:190:3064::6]:5683".parse().unwrap();

    use coap_request::Stack;
    println!("Sending GET to {}...", demoserver);
    let response = client
        .to(demoserver)
        .request(
            coap_request_implementations::Code::get()
                .with_path("/other/separate")
                .processing_response_payload_through(|p| {
                    println!("Got payload {:?}", p);
                }),
        )
        .await;
    println!("Response {:?}", response);
}
