//! blinq example
//!
//! This demo shows how any unrelated no_std crate can be used, by the example of blinq (a morse
//! code and other pattern blinker).
#![no_std]

use riot_wrappers::riot_main;

use embedded_hal::blocking::delay::DelayMs;

use riot_wrappers::{
    ztimer,
};

riot_main!(main);

fn main() {
    let mut delay = ztimer::Clock::msec();

    let led0 = riot_wrappers::led::LED::<0>::new();
    // A short queue: we're feeding the letters in byte-wise here, always followed by a letter
    // space. (If larger preset messages were fed, it'd need to be as long as them).
    let mut blinq: blinq::Blinq<_, 2> = blinq::Blinq::new(led0, false);

    let mut stdio = riot_wrappers::stdio::Stdio {};

    const LETTERSPACE: blinq::Pattern = blinq::Pattern::from_u32(0, 2); // 3, but the dashes and dots of the letters all include one already
    const WORDSPACE: blinq::Pattern = blinq::Pattern::from_u32(0, 2); // 7, but the 3 already handled by the last letter, and 2 because the processor will append a letterspace

    let mut buf = [0; 10];
    loop {
        let buf = stdio.read_raw(&mut buf);
        if let Ok(buf) = buf {
            for b in buf {
                use blinq::patterns::morse::*;

                let letterpattern = match *b as char {
                    'a' | 'A' => A,
                    'b' | 'B' => B,
                    'c' | 'C' => C,
                    'd' | 'D' => D,
                    'e' | 'E' => E,
                    'f' | 'F' => F,
                    'g' | 'G' => G,
                    'h' | 'H' => H,
                    'i' | 'I' => I,
                    'j' | 'J' => J,
                    'k' | 'K' => K,
                    'l' | 'L' => L,
                    'm' | 'M' => M,
                    'n' | 'N' => N,
                    'o' | 'O' => O,
                    'p' | 'P' => P,
                    'q' | 'Q' => Q,
                    'r' | 'R' => R,
                    's' | 'S' => S,
                    't' | 'T' => T,
                    'u' | 'U' => U,
                    'v' | 'V' => V,
                    'w' | 'W' => W,
                    'x' | 'X' => X,
                    'y' | 'Y' => Y,
                    'z' | 'Z' => Z,
                    ' ' | '\n' => WORDSPACE,
                    '.' => FULL_STOP,
                    _ => ERROR,
                };

                blinq.enqueue(letterpattern);
                blinq.enqueue(LETTERSPACE);

                while !blinq.idle() {
                    blinq.step();
                    delay.delay_ms(150);
                }
            }
        }
    }
}
