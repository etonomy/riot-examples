#![no_std]
//! This example shows the use of ZTimer for more than just sleeping.
//!
//! As the ZTimer abstraction gains more features, some of this may be moved into commands.

use riot_wrappers::riot_main;

riot_main!(main);

fn main() {
    use riot_wrappers::mutex::Mutex;
    use riot_wrappers::println;
    use riot_wrappers::ztimer::{Clock, Ticks};

    let sec = Clock::sec();

    let mutex = Mutex::new(());
    let keep_running = mutex.lock();

    // A task can't simply be aborted in RIOT (or C -- without putting it into a well-abortable
    // operating system thread, which RIOT threads aren't really designed to work like); instead, a
    // useful pattern for timing out is to do a cheap check whehter an abort is indicated
    // repeatedly through the process, and exit then.
    //
    // The check here is done by trying to get a mutex, a lock on which is moved into a timer.

    sec.set_during(
        || drop(keep_running),
        Ticks::from_duration(core::time::Duration::from_secs(5))
            .expect("5 would only overflow a nanosecond timer"),
        || {
            println!("I'm starting extensive calculations...");
            loop {
                println!("Running through a step");
                // Not really, just sleeping again
                sec.sleep_ticks(1);
                if mutex.try_lock().is_some() {
                    println!("Oh, I ran into a timeout, I better stop now");
                    break;
                }
            }
        },
    );
}
