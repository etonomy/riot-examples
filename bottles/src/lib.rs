#![no_std]

use riot_wrappers::riot_main;

use embedded_hal::blocking::delay::DelayMs;

use riot_wrappers::{
    ztimer,
    stdio::println,
};

// Wrap main into a C function named `int main(void)` function that is invoked as the main thread
// function by RIOT.
riot_main!(main);

fn main() {
    let mut delay = ztimer::Clock::msec();

    let mut bottles = 99;

    while bottles > 0 {
        delay.delay_ms(1000);
        println!("{} bottles of beer on the wall, {} bottles of beer", bottles, bottles);
        delay.delay_ms(200);
        bottles -= 1;
        println!("Take one down, pass it around: {} bottles of beer", bottles);
    }
}
