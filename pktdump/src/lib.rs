//! An example packet dumper
//!
//! This registers the main thread for reception of any messages of any type in any context (no
//! port / protocol limitations).
//!
//! The msg wrappers ensure that the received messages' pointers are cast to the Pktsnip type that
//! properly drops them after they have been displayed (or would even drop them if they had not
//! been processed), and the registration function's types contain all information needed to split
//! the right message number off the pool of available message numbers.
#![no_std]

use riot_wrappers::riot_main;

use riot_wrappers::{
    gnrc::netreg,
    msg::v2::MessageSemantics,
    stdio::println,
    thread,
};

riot_main!(main);

fn main(tok: thread::StartToken) -> ((), thread::EndToken) {
    tok.with_message_queue::<4, _>(|tok| {
        let (_tok, semantics) = tok.take_msg_semantics();

        let (semantics, recv_rx, recv_tx) = semantics.split_off();

        netreg::register_for_messages(
            recv_tx,
            riot_sys::gnrc_nettype_t_GNRC_NETTYPE_UNDEF,
            riot_sys::GNRC_NETREG_DEMUX_CTX_ALL,
            || loop {
                semantics
                    .receive()
                    .decode(&recv_rx, |sender, p| {
                        println!("Received from {:?}: {:?}", sender, p);
                        for part in p.iter_snips() {
                            println!("Snip {:?}", part);
                        }
                    })
                    .unwrap();
            },
        );
    })
}
