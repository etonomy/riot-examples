//! Demo and tests around the riot_wrappers::msg::v2 abstraction
//!
//! This currently performs both the roles of a demo (doing the simple things) and tests (doing the
//! odd things); parts of the code are commented thusly.
#![no_std]

use riot_wrappers::riot_main_with_tokens;
use riot_wrappers::println;
use riot_wrappers::thread;

use riot_wrappers::msg::v2 as msg;
use riot_wrappers::msg::v2::MessageSemantics;

use riot_sys;

riot_main_with_tokens!(outer_main);

// Test struct to illustrate when messages are dropped.
#[derive(Debug)]
struct LoudlyDrop {
    a: u8,
    b: u8,
}

impl Drop for LoudlyDrop {
    fn drop(&mut self) {
        println!("Dropping {:?} now.", self);
    }
}

// Using an "outer" to not re-indent the whole main function
fn outer_main(initial: thread::StartToken) -> ((), thread::TerminationToken) {
    initial.with_message_queue::<4, _>(|initial| {
        // Doing this in the closure because the current type if `initial` can't be named, and that
        // would be required when using it with a function
        let (_, semantics) = initial.take_msg_semantics();
        main(semantics)
    })
}

fn main(message_semantics: msg::NoConfiguredMessages) -> ! {
    // Declaring pairs of types and message numbers that will go togeher.
    type HelloPort = msg::ReceivePort<(), 42>;
    type HelloPort2 = msg::ReceivePort<&'static u64, 44>;
    type LoudDropPort = msg::ReceivePort<LoudlyDrop, 23>;

    // As long as hello and hello2 live, this threads will accept type/number pairs as declared.
    //
    // In many API use cases, the type annotations should not be necessary, because the receiving
    // end of the port will be fixed on a port type.
    //
    // Doing this twice with ports of the same type will result in a runtime error, which (with
    // constificaction of the language) should eventually become a compile time error. Either way,
    // the checks themselves are performed at build time with sufficient optimization.
    let (message_semantics, hello, hello_send): (_, HelloPort, _) = message_semantics.split_off();
    let (message_semantics, hello2, hello2_send): (_, HelloPort2, _) = message_semantics.split_off();
    let (message_semantics, loudly, loudly_send): (_, LoudDropPort, _) = message_semantics.split_off();

    println!("You may now send me messages on {:?}", hello2);

    fn use_in_other_thread<T: 'static + Send + Sync>(t: T) -> T{
        // Not relly doing anything, but if this builds we know we could really pass out tickets.
        t
    }
    let hello_send = use_in_other_thread(hello_send);

    // This will indicate success if there's a sufficiently long queue, or err back if not.
    riot_wrappers::dbg!(loudly_send.try_send(LoudlyDrop { a: 1, b: 8 }));
    riot_wrappers::dbg!(hello2_send.try_send(&bignum));
    riot_wrappers::dbg!(hello_send.try_send(()));
    riot_wrappers::dbg!(hello2_send.try_send(&10));
    riot_wrappers::dbg!(loudly_send.try_send(LoudlyDrop { a: 2, b: 3 }));

    // Test: Manually constructing a message to be sent, and through an interrupt furthermore.
    //
    // This allows playing with unknown types, or with content that fails to cast back. (Putting a
    // `value: 5` in there while on the &u64 type will cause a segfault when decoded).
    static bignum: u64 = 5;
    // These need to live until timeout is over!
    let mut t: riot_sys::ztimer_t = Default::default();
    let mut m = riot_sys::msg_t {
        sender_pid: 99,
        type_: 44,
        content: riot_sys::msg_t__bindgen_ty_1 { ptr: &bignum as *const _ as *mut _ },
    };
    unsafe { riot_sys::ztimer_set_msg(riot_sys::ZTIMER_SEC, &mut t, 4, &mut m, riot_sys::thread_getpid() as _) };

    println!("All set up, processing any incoming messages in a loop now...");
    println!("");

    loop {
        let code = message_semantics.receive()
            .decode(&hello, |s, ()| {
                println!("Hello received from {:?}", s);
                "h"
            })
            .or_else(|m| m.decode(&hello2, |s, n| {
                println!("Number {} received from {:?}", n, s);
                "n"
            }))
            .or_else(|m| m.decode(&loudly, |s, l| {
                println!("Received {:?}, which will soon make more noise", l);
                "l"
            }))
            .unwrap_or_else(|m| {
                // Given the above is exhaustive of the created ports, this won't happen, and we
                // could just as well .unwrap() -- but comment out the or_else part and this turns
                // up.
                //
                // This is *also* executed when the message type isn't known; if that's the case,
                // it will panic when dropping. (You can test that by setting an unknown type_ in
                // the above ztimer).
                //
                // If we don't want special handling, this branch can be removed: known messages
                // will be ignored, but still unknowns cause a panic when the result is dropped.
                //
                // (If we were very sure we don't receive bad messages, and OK with leaking ones we
                // didn't decode, we could also core::mem::forget(m) here and suffer no checking
                // code at all.)
                println!("A message was received that was not previously decoded; we're dropping it.");
                "x"
            })
            ;
        // Returning something from the decoders is not something expected to be common, but it's
        // possible as long as all decoders return alike, and provides better static checks than
        // `let mut result = None;` and assigning to result in the decode closures. (If not used,
        // the closures can just all implicitly return () as any trailing semicolon does).
        println!("Result code {:?}", code);
    }
}
