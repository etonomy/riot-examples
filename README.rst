This is a collection of example applications written in Rust_ that use the
`RIOT Operating System`_ via the `riot-wrappers`_ crate.

For examples in the style of Rust libraries that are on their path to becoming
RIOT modules, see the `riot-module-examples`_ repository.

For general getting-started instructions for using Rust on RIOT see the `RIOT documentation`_;
the OS also contains two minimal examples of its own.
This repository contains advanced examples,
and is used to illustrate more of the riot-wrappers abstractions.

.. _Rust: https://www.rust-lang.org/
.. _`RIOT Operating System`: https://riot-os.org/
.. _`riot-wrappers`: https://gitlab.com/etonomy/riot-wrappers
.. _`riot-module-examples`: https://gitlab.com/etonomy/riot-module-examples/
.. _`RIOT documentation`: https://doc.riot-os.org/using-rust.html

Examples
--------

* bottles: A '99 bottles of beer' program that just prints the device's debug
  output for some time.

  This is used as a first example not because it's particularly
  microcontroller-ish, but because it can be run both on any board that has a
  debug UART and on the native board under Linux::

      $ cd bottles
      $ make BOARD=native all term
      [read the text or abort with Ctrl-C]

* saul_blink: A program that enumerates a device's LEDs at runtime using RIOT's
  Sensor Actor Über Layer (SAUL), and toggles them.

  Both for demo purposes and to ensure some meaningful action even on the native board,
  it also registers a "software LED" that,
  rather than actually lighting up,
  reports its status changes via stdout.

* shell_threads: A demo of the shell integration and using multiple therads.
  Run it with::

    $ make BOARD=stk3700 flash term

  or::

    $ make BOARD=native all term

  and enter ``help`` to list the available shell commands.

Further documentation
---------------------

To retrace the examples and to write own applications,
see the `riot-wrappers documentation`_ for the safe wrappers,
and the `riot-sys documentation`_ for accessing arbitrary RIOT functions.
(Or, almost arbitrary: as outlined in its Extension docs section,
headers that are not exported yet may need to be added to its ``riot-headers.h`` file).

.. _`riot-wrappers documentation`: https://rustdoc.etonomy.org/riot_wrappers/
.. _`riot-sys documentation`: https://rustdoc.etonomy.org/riot_sys/

License
-------

This crate is licensed under the same terms as of the LGPL 2.1, following the
license terms of the RIOT Operating System.

It is maintained by Christian M. Amsüss <ca@etonomy.org> as part of the etonomy
project, see <https://etonomy.org/>.
