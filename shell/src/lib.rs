#![no_std]
//! This example enables a lot of modules, and offers RIOT's shell commands in parallel with Rust
//! implementations from the accompanying riot-modules-examples' riot-shell-commands crate.

use riot_wrappers::riot_main;
use riot_wrappers::shell::CommandList;

riot_main!(main);

fn main() {
    let mut commands = riot_shell_commands::all();
    commands
        .with_buffer_size::<32>()
        .run_forever_providing_buf();
}
