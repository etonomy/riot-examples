#![no_std]
//! This example runs a shell in a dedicated thread, and moves data around between the threads
//! using a mutex.
//!
//! One of the purposes of this example is to show how mutexes can be used; it makes no claims on
//! the suitability of this mechanism for actually interacting with countdowns.

use core::fmt::Write;

use riot_wrappers::riot_main;
use riot_wrappers::cstr::cstr;

use embedded_hal::delay::DelayNs;

use riot_wrappers::{
    ztimer,
    thread,
    mutex::Mutex,
    stdio::println,
    shell,
};

riot_main!(main);

static SECONDTHREAD_STACK: Mutex<[u8; 2048]> = Mutex::new([0; 2048]);

fn main() {
    let countdown = Mutex::new(11);

    let mut secondthread_main = || {
        let mut delay = ztimer::Clock::msec();
        println!("Second thread is running");

        loop {
            delay.delay_ms(3000);
            let mut locked = countdown.lock();
            if *locked > 0 {
                *locked -= 1;
                println!("This is the second thread, counting {}", *locked);
            }
        }
    };
    // One can either allocate the stack for the new thread statically (and ensure via a mutex that
    // it's only used once), or...
    let mut secondthread_stacklock = SECONDTHREAD_STACK.lock();

    // ... or plainly allocate it on another thread's stack. In both cases, usually, the
    // spawning thread must outlive the child. (In the former, there might be the option to move
    // out Thread object and the stack reference).
    let mut shellthread_stack = [0u8; 5120];
    let mut shellthread_mainclosure = || shellthread_main(&countdown);

    thread::scope(|threadscope| {

    let secondthread = threadscope.spawn(
        secondthread_stacklock.as_mut(),
        &mut secondthread_main,
        cstr!("secondthread"),
        (riot_sys::THREAD_PRIORITY_MAIN - 2) as _,
        (riot_sys::THREAD_CREATE_STACKTEST) as _,
    ).expect("Failed to spawn second thread");

    println!("Second thread spawned as {:?} ({:?}), status {:?}", secondthread.pid(), secondthread.pid().get_name(), secondthread.status());

    let shellthread = threadscope.spawn(
        shellthread_stack.as_mut(),
        &mut shellthread_mainclosure,
        cstr!("shellthread"),
        (riot_sys::THREAD_PRIORITY_MAIN - 1) as _,
        (riot_sys::THREAD_CREATE_STACKTEST) as _,
    ).expect("Failed to spawn shell thread");

    println!("Shell thread spawned as {:?} ({:?}), status {:?}", shellthread.pid(), shellthread.pid().get_name(), shellthread.status());

    // Terminating the main thread will lead to some serious quarrel from the destructors of
    // thread, for we can't just std::process::Child::wait() on them to free up anything we'd have
    // passed in -- so rather than doing this loop at the ensuring panic, we enter it deliberately.
    loop {
        thread::sleep();
    }

    // If we knew they had terminated, we could just collect them; see
    // riot_wrappers::thread::CountingThreadScope::reap documentation for why that doesn't block.
    // threadscope.reap(secondthread);
    // threadscope.reap(shellthread);
    // Reaching here without having called the reaps would panic, but we have an endless loop
    // before anyway
    });

    unreachable!();
}

fn shellthread_main(countdown: &Mutex<u32>) -> ! {
    // This is the lock that's held during countdown pauses. As commands may take mutable closures,
    // no synchronization is necessary -- CommmandList wrappers ensure the compiler that no two
    // commands will be run at the same time.
    let mut own_lock = None;

    use riot_wrappers::shell::CommandList;

    shell::new()
        .and(cstr!("ps.rs"), cstr!("List the processes"),
            |stdio: &mut _, _args: shell::Args| riot_shell_commands::ps(stdio))
        .and(cstr!("sleep"), cstr!("Pause the shell prompt"),
            |stdio: &mut _, args: shell::Args| riot_shell_commands::sleep(stdio, args.iter()))
        .and(
            cstr!("countdown"),
            cstr!("Set or pause the countdown timer in the other thread"), move |stdio: &mut _, args: shell::Args| {
                // It'd be really tempting to have a UsageError here that implements Debug and can
                // thus be used as a Termination error, but the result of a command can't currently
                // capture its arguments.
                let mut usage = || {
                    writeln!(stdio, "usage: {} [pause|unpause|number]", &args[0]).unwrap();
                };

                if args.len() == 2 {
                    match (&args[1], args[1].parse()) {
                        (_, Ok(num)) => {
                            let mut lock = own_lock.take().unwrap_or_else(|| countdown.lock());
                            *lock = num;
                        }
                        ("pause", _) => {
                            if own_lock.is_none() {
                                own_lock = Some(countdown.lock());
                            }
                        }
                        ("unpause", _) => {
                            own_lock = None;
                        }
                        _ => return usage(),
                    }
                } else if args.len() == 1 {
                    match &own_lock {
                        None => writeln!(stdio, "Second thread can work, currently at {}.", *countdown.lock()).unwrap(),
                        Some(lock) => writeln!(stdio, "Second thread is frozen at {}.", **lock).unwrap(),
                    }
                } else {
                    return usage();
                }
            }
        )
        .run_forever()
}
