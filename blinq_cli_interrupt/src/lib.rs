//! blinq example
//!
//! This demo shows how any unrelated no_std crate can be used, by the example of blinq (a morse
//! code and other pattern blinker), and uses the code in an interrupt context.
#![no_std]

use core::time::Duration;

use riot_wrappers::riot_main;

use riot_wrappers::{
    ztimer,
    ztimer::periodic,
    println,
};

riot_main!(main);

fn main() {
    let led0 = riot_wrappers::led::LED::<0>::new();
    // A relatively long queue: Patterns are fed by the main thread into here, and the main thread
    // does not start blocking just because the blinks aren't out yet.
    let blinq: blinq::Blinq<_, 16> = blinq::Blinq::new(led0, false);

    let mut stdio = riot_wrappers::stdio::Stdio {};

    const LETTERSPACE: blinq::Pattern = blinq::Pattern::from_u32(0, 2); // 3, but the dashes and dots of the letters all include one already
    const WORDSPACE: blinq::Pattern = blinq::Pattern::from_u32(0, 2); // 7, but the 3 already handled by the last letter, and 2 because the processor will append a letterspace

    struct BlinqTick(blinq::Blinq<riot_wrappers::led::LED<0>, 16>);
    impl periodic::Handler for BlinqTick {
        fn trigger(&mut self) -> periodic::Behavior {
            self.0.step();
            if self.0.idle() {
                periodic::Behavior::Abort
            } else {
                periodic::Behavior::KeepGoing
            }
        }
    }
    let mut timer = core::pin::pin!(periodic::Timer::new(
        ztimer::Clock::msec(),
        BlinqTick(blinq),
        Duration::from_millis(150).try_into().expect("short enough")
        ));
    timer.start();

    let mut buf = [0; 10];
    loop {
        let buf = stdio.read_raw(&mut buf);
        if let Ok(buf) = buf {
            for b in buf {
                use blinq::patterns::morse::*;

                let letterpattern = match *b as char {
                    'a' | 'A' => A,
                    'b' | 'B' => B,
                    'c' | 'C' => C,
                    'd' | 'D' => D,
                    'e' | 'E' => E,
                    'f' | 'F' => F,
                    'g' | 'G' => G,
                    'h' | 'H' => H,
                    'i' | 'I' => I,
                    'j' | 'J' => J,
                    'k' | 'K' => K,
                    'l' | 'L' => L,
                    'm' | 'M' => M,
                    'n' | 'N' => N,
                    'o' | 'O' => O,
                    'p' | 'P' => P,
                    'q' | 'Q' => Q,
                    'r' | 'R' => R,
                    's' | 'S' => S,
                    't' | 'T' => T,
                    'u' | 'U' => U,
                    'v' | 'V' => V,
                    'w' | 'W' => W,
                    'x' | 'X' => X,
                    'y' | 'Y' => Y,
                    'z' | 'Z' => Z,
                    ' ' | '\n' => WORDSPACE,
                    '.' => FULL_STOP,
                    _ => ERROR,
                };

                let restart = timer.alter(|blinq| {
                    let restart = blinq.0.idle();
                    blinq.0.try_enqueue(letterpattern)
                        .unwrap_or_else(|_| println!("Overflow!"));
                    blinq.0.try_enqueue(LETTERSPACE)
                        .unwrap_or_else(|_| println!("Overflow!"));
                    restart
                });
                if restart {
                    timer.start();
                }
            }
        }
    }
}
